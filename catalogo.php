<?php include("header.php"); ?>
<body class="font-n">

<!-- Main Content -->
	<!-- Menu Bar -->
	<div class="col-xs-12 static">
		<h1 class="font-sb pos-logo mt0 white"><i>UnlockSpaces <span class="woody"><h3>WoodyStyle</h3></span></i></h1>
		<div class="pos-log">
				<a href="#"><h5 class="intro-button inline-b font-l l-white">Register</h5></a>
				&nbsp;&nbsp;&nbsp;
				<a href="#"><h5 class="intro-button inline-b font-l l-white">Login</h5></a>
				&nbsp;&nbsp;&nbsp;
				<a href="catalogo.php" class="go-contacto"><h5 class="intro-button inline-b font-l l-white">Catálogo</h5></a>
			</div>
	</div>

	<!-- Searc Field -->
	<div class="col-xs-12">
		<h1 class="text-center mt100 mb0 font-sb">¡Encuentra tu lugar ideal, resérvalo y listo!</h1>
		<h4 class="text-center font-l">¡Busca el precio que más se ajuste a tu bolsillo!</h4>

		

		<h3 class="pleft40 mt50"><i class="fa fa-star brown"></i> Destacados</h3>
		<div class="col-xs-12 pads0 mt20">
			<div class="col-md-3 mb50" align="center">
				<img src="imx/featured-1.jpg" class="img-responsive h200" alt="">
				<p class="mt20">Excelente sala de reuniones ambientado en madera de roble, cubriendo además todas las necesidades requeridas para reuniones o video conferencias.</p>
				<h5 class="font-sb">Precio: $5.500 / Hora</h5>
				<a href="espacio-1.php" class="l-black font-sb"><h4>Ver espacio</h4></a>
			</div>
			<div class="col-md-3 mb50" align="center">
				<img src="imx/featured-2.jpg" class="img-responsive h200" alt="">
				<p class="mt20">Cómoda sala de espera, indicada para aquellos que necesiten un lugar cómodo mientras esperan para ser atendidos, cuenta con diferentes ambientes.</p>
				<h5 class="font-sb">Precio: $12.000 / Hora</h5>
				<a href="#" class="l-black font-sb"><h4>Ver espacio</h4></a>
			</div>
			<div class="col-md-3 mb50" align="center">
				<img src="imx/featured-3.jpg" class="img-responsive h200" alt="">
				<p class="mt20">Perfectas estaciones de trabajo para aquellos que trabajan en equipo y necesiten de un lugar cómodo en donde necesiten que sus ideas fluyan, ambientizado con madera de pino.</p>
				<h5 class="font-sb">Precio: $15.000 / Hora</h5>
				<a href="#" class="l-black font-sb"><h4>Ver espacio</h4></a>
			</div>
			<div class="col-md-3 mb50" align="center">
				<img src="imx/featured-4.jpg" class="img-responsive h200" alt="">
				<p class="mt20">Especial para trabajar y descansar, esta estación de trabajo / sala de relajación combina a la perfección lo que nosotros llamamos "un trabajo relajado".</p>
				<h5 class="font-sb">Precio: $7.800 / Hora</h5>
				<a href="#" class="l-black font-sb"><h4>Ver espacio</h4></a>
			</div>
		</div>
	</div>
	



<?php include("footer.php"); ?>